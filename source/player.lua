 
player = {}
local spaceship = {}
local enemyship = {}
local spaceshipBulletsShot = {}
local spaceshipBulletsShot = {}
local enemies = {{},{},{},{},{},{},{},{},{},{}}
local enemiesNumber = 0
local enemyBulletsShot = {}
local bangs = {}
local bangsi = {}
local score = 0
local tiro= {}
function player.load()
 player.bullets={};

 enemyship.shootingSpeed = 0.3
 enemyship.width = 50
 enemyship.height = 47
 enemyship.shotProb = 20
 spaceship.lastShot = 0

 spaceship.speed = 150
 spaceship.shootingSpeed = 0.35
 spaceship.bulletSpeed = 250
 spaceship.width = 75
 spaceship.height = 71
 spaceship.lastShot = 0
 spaceship.x = love.graphics.getWidth() / 2 - spaceship.width/2
	spaceship.y = love.graphics.getHeight() - 100
	bangsi.image = love.graphics.newImage('imagens/bangs.png')
	bangsi.greenQuad = love.graphics.newQuad(0, 0, 40, 27, 40, 108)
	bangsi.yellowQuad = love.graphics.newQuad(0, 27, 40, 27, 40, 108)
	bangsi.blueQuad = love.graphics.newQuad(0, 54, 40, 27, 40, 108)
	bangsi.redQuad = love.graphics.newQuad(0, 81, 40, 27, 40, 108)
 enemyship.image = love.graphics.newImage('imagens/enemyshipgreen.png')
 enemyship.bang = bangsi.blueQuad
 enemyship.speed = 50
	enemyship.shootingSpeed = 0.3
	enemyship.bang = bangsi.blueQuad
	enemyship.bullet = love.graphics.newQuad(0, 19, 10, 8, 10, 36)
 player.img = love.graphics.newImage("imagens/spaceship.png")
 player.width = 50;
 player.heigth = 100;
 player.x = 100;
 player.y = 450;
 player.cooldown = 10
 player.speed = 300;
  tiro = love.graphics.newImage('imagens/bullets.png');
 player.fire_sound=love.audio.newSource('sounds/laser.wav');
 player.setEnemies()
end

 function player.setEnemies()
	enemiesNumber = 0;
	for i=1,4 do
		for j=1,10 do
			local enemy = {}
			enemy.x = (j-1) * enemyship.width + 10
			enemy.y = (i-1) * enemyship.height + 35
			if level == 3 then
				enemy.health = 3
			end
			table.insert(enemies[j],enemy)
			enemiesNumber = enemiesNumber + 1
		end
	end
end
function player.update(dt)
    player.cooldown = player.cooldown - 0.8
    
	local tmp = player.x;
	
	if love.keyboard.isDown("left") then 	
		player.x = player.x - (player.speed * dt)     
    elseif love.keyboard.isDown("right") then  	 	
		player.x = player.x +(player.speed * dt)
	end	
	
	if player.x < 0 then
		player.x = tmp
	elseif player.x > 700 then
	    player.x = tmp
	end
	
	if love.keyboard.isDown("space")then
		player.fire()
	end
	for i,b in ipairs(player.bullets) do
		if b.y < -10 then
			table.remove(player.bullets,i)
		end	
		b.y= b.y - 5
	end
	--player.spaceshipShoot()
	player.spaceshipShots(dt)
	player.enemyShots(dt)
	if next(enemyBulletsShot) ~= nil then
		if love.math.random(1,100) < enemyship.shotProb and enemyship.lastShot > enemyship.shootingSpeed then
				local r = love.math.random(1,#enemies)
				if next(enemies[r]) ~= nil then
					enemyShoot(enemies[r][#enemies[r]].x,enemies[r][#enemies[r]].y)
				end
		end
	end
	if directionRight then
			for i=1,#enemies do
				for j=1,#enemies[i] do
					enemies[i][j].x = enemies[i][j].x + dt * enemyship.speed
					if enemies[i][j].x + enemyship.width + 2 >= love.graphics.getWidth() then
						directionRight = false
						enemyship.down = enemyship.downDistance
					end
				end
			end
		else
			for i=1,#enemies do
				for j=1,#enemies[i] do
					enemies[i][j].x = enemies[i][j].x - dt * enemyship.speed
					if enemies[i][j].x - 2 <= 0 then
						directionRight = true
						enemyship.down = enemyship.downDistance
					end
				end
			end
		end

	
end
function player.enemyShots(dt)
	if next(enemyBulletsShot) ~= nil then
		for i=#enemyBulletsShot,1,-1 do
			enemyBulletsShot[i].y = enemyBulletsShot[i].y + dt * enemyship.bulletSpeed
			if enemyBulletsShot[i].y > love.graphics.getHeight() then
				table.remove(enemyBulletsShot,i)
			elseif spaceship.x <= enemyBulletsShot[i].x and spaceship.x + spaceship.width >= enemyBulletsShot[i].x
				and spaceship.y <= enemyBulletsShot[i].y and spaceship.y + spaceship.height >= enemyBulletsShot[i].y then
				table.insert(bangs, {['x'] = spaceship.x, ['y'] = spaceship.y, ['dt'] = 0.1})
				table.remove(enemyBulletsShot, i)
				spaceshipShot()
			end
		end
	end
end

function player.fire()
    if player.cooldown <= 0 then
	    love.audio.play(player.fire_sound)
		player.cooldown = 10        
		bullet={}
		bullet.x= player.x + 46
		bullet.y= player.y + 42
		table.insert(player.bullets,bullet)
	end
	
end
 function player.enemyShoot(x,y)
	local bullet = {}
	enemyship.lastShot = 0
	bullet.x = x + enemyship.width/2
	bullet.y = y - 5
	table.insert(enemyBulletsShot, bullet)
end

 function player.spaceshipShots(dt)
	if next(spaceshipBulletsShot) ~= nil then
		for i=#spaceshipBulletsShot,1,-1 do
			spaceshipBulletsShot[i].y = spaceshipBulletsShot[i].y - dt * spaceship.bulletSpeed
			if spaceshipBulletsShot[i].y < 0 then
				table.remove(spaceshipBulletsShot,i)
				-- bad shots cost 5 points
				score = score - 5
			else
				for j=#enemies,1,-1 do
					for k=#enemies[j],1,-1 do
						if next(spaceshipBulletsShot) ~= nil and spaceshipBulletsShot[i] ~= nil and 
								enemies[j][k].x <= spaceshipBulletsShot[i].x and enemies[j][k].x + 50 >= spaceshipBulletsShot[i].x
								and enemies[j][k].y <= spaceshipBulletsShot[i].y and enemies[j][k].y + 47 >= spaceshipBulletsShot[i].y then
							table.insert(bangs, {['x'] = enemies[j][k].x, ['y'] = enemies[j][k].y, ['dt'] = 0.2})
							table.remove(spaceshipBulletsShot, i)
							if level == 3 and enemies[j][k].health > 0 then
								enemies[j][k].health = enemies[j][k].health - 1
							else
								table.remove(enemies[j], k)
								enemiesNumber = enemiesNumber - 1
								score = score + 50
							end
							break
						end
					end
				end
			end
		end
	end
end
 function player.spaceshipShoot()
	local bullet = {}
	spaceship.lastShot = 0
	bullet.x = spaceship.x + 32
	bullet.y = spaceship.y + 10
	table.insert(spaceshipBulletsShot, bullet)
end
function player.draw()

 love.graphics.draw(player.img, player.x,player.y)
 
 love.graphics.setColor(255,255,255)	
 love.graphics.setColor(255,0,0) 
 for _,b in pairs(player.bullets) do
		love.graphics.circle("fill", b.x, b.y, 6)	
 end
 love.graphics.setColor(255,255,255) 
 
 if next(enemyBulletsShot) ~= nil then
		for i=1,#enemyBulletsShot do
			love.graphics.draw(tiro, enemyship.bullet, enemyBulletsShot[i].x, enemyBulletsShot[i].y)
		end
 end
	
-- enemies
 if next(enemies) ~= nil then
	for i=1,#enemies do
		for j=1,#enemies[i] do
			love.graphics.draw(enemyship.image, enemies[i][j].x, enemies[i][j].y)
		end
	end
 end
 
 	if next(bangs) ~= nil then
		for i=1,#bangs do
			love.graphics.draw(bangsi.image, enemyship.bang, bangs[i].x, bangs[i].y)
		end
	end
 
end

