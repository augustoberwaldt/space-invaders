require('source/player')
--[load codigo --]
function love.load()
   local sound = love.audio.newSource('sounds/02-las-vegas.mp3' , "static")
   love.audio.play(sound)
   image = love.graphics.newImage("imagens/336006.png")
   quad = love.graphics.newQuad(0, 0, 900, 700, image:getWidth(), image:getHeight())
   player.load();
end

function love.update(dt)
   player.update(dt);
end

function love.draw() 
   love.graphics.draw(image,quad)
   player.draw();
end